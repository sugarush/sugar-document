__author__ = 'lux'

from setuptools import setup

setup(
    name='sugar-document',
    version='0.0.1',
    author='lux',
    author_email='lux@sugarush.io',
    url='https://gitlab.com/sugarush/sugar-document',
    packages=['sugar_document'],
    description='Map dictionary keys to object attributes.',
    install_requires=[ ]
)
